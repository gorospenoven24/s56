import React from 'react';
import { Fragment } from 'react'
import Navbar from 'react-bootstrap/Navbar';
import { Container} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import style from '../App.css'



export default function AppNavbar(){
	return (
				<div style={{backgroundColor:"#f02d1f", color:"white"}}>
					<Navbar collapseOnSelect expand="lg"  variant="dark">
		                <Navbar.Brand as={NavLink} to="/">ShopNow</Navbar.Brand>
		                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		                <Navbar.Collapse id="responsive-navbar-nav">
		                <Nav>
		                        <Nav.Link as={NavLink} to="/">Product List</Nav.Link>
		                        <Nav.Link >Category</Nav.Link>
		                        <Nav.Link href="">About</Nav.Link>
		                          <Nav.Link as={NavLink} to="/admin">"Temporary link for Admin"</Nav.Link>
		                    </Nav>
		                    <Nav className="ml-auto">
		                        <Nav.Link href="">Cart</Nav.Link>
		                        <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
		                        <Nav.Link  as={NavLink} to="/register">Sign Up</Nav.Link>
		                         <Nav.Link   href="">Help</Nav.Link>
		                    </Nav>
		                </Navbar.Collapse>
		           	 </Navbar>
		            </div>

	
		)	
}