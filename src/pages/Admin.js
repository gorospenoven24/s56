import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Table from 'react-bootstrap/Table'
// import Shop from '../Image/Shop.jpg';


export default function Admin(){
	return (
		<Fragment>
		<Container>
			 <h3 className= "mt-5">Welcome Admin</h3>
		</Container>
		<Container fluid className= "pl-5">
		  <Row>
		    <Col sm={3}>
		  		<ButtonGroup vertical className="buttonGroup">
		  		  <Button className="adminButton">Product List</Button>
		  		  <Button className="adminButton">User's List</Button>
		  		  <Button className="adminButton">Category</Button>
		  		   <Button className="adminButton">Orders</Button>
		  		  <Button className="adminButton">Log out</Button>
		  		</ButtonGroup>
		    </Col>
		    <Col sm={9}>
		    <Button className="controlButton" variant="primary" size="lg">
		       Add 
		      </Button>

		       <Button  className="controlButton"  variant="primary" size="lg">
		       Edit
		      </Button>

		        <Button  className="controlButton"  variant="primary" size="lg">
		        Delete
		      </Button>
		    <hr className="hr"></hr>
		    <Table striped bordered hover size="sm">
		      <thead>
		        <tr>
		          <th>#</th>
		          <th>First Name</th>
		          <th>Last Name</th>
		          <th>Username</th>
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		          <td>1</td>
		          <td>Mark</td>
		          <td>Otto</td>
		          <td>@mdo</td>
		        </tr>
		        <tr>
		          <td>2</td>
		          <td>Jacob</td>
		          <td>Thornton</td>
		          <td>@fat</td>
		        </tr>
		        <tr>
		          <td>3</td>
		          <td colSpan="2">Larry the Bird</td>
		          <td>@twitter</td>
		        </tr>
		      </tbody>
		    </Table>
		    </Col>
		  </Row>
		</Container>
		</Fragment>
	)
}