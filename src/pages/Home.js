import React from 'react';
import { Fragment } from 'react'
import Navbar from 'react-bootstrap/Navbar';
import { Container} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import Banner from '../components/Banner'
import SecondNavbar from '../components/SecondNavbar';



export default function Product(){
	return (
		<Fragment>
		 <SecondNavbar />

		<Container fluid>
		
		 <Banner />
		<Row className = "mt-3 mb-3">
			<Col xs={12} md={3} className="pt-3">
				<Card style={{ width: '18rem' }} className="cardStyle">
				  <Card.Img className="imgCard"variant="top" src="https://assets.hermes.com/is/image/hermesproduct/tote-bag--1462298%2092-front-1-300-0-850-850_b.jpg" />
				  <Card.Body>
				    <Card.Title>Slim bag</Card.Title>
				    <Card.Text>
				    Descrition: Shoulder bag
				    </Card.Text>
				     <Card.Text>
				    php 200
				    </Card.Text>
				    <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3}  className="pt-3">
				<Card style={{ width: '18rem' }}>
				  <Card.Img  className="imgCard" variant="top" src="https://media.everlane.com/image/upload/c_fill,dpr_2,f_auto,g_face:center,q_auto,w_auto/v1/i/1f40946d_e158.jpg" />
				  <Card.Body>
				    <Card.Title>Black Jean </Card.Title>
				    <Card.Text>
				    Descrition: Pants
				    </Card.Text>
				     <Card.Text>
				    php 300
				    </Card.Text>
				     <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3}  className="pt-3">
				<Card style={{ width: '18rem' }}>
				  <Card.Img  className="imgCard"  variant="top" src="https://rukminim1.flixcart.com/image/714/857/kb5eikw0/jean/k/z/p/30-ekjn002341-ecko-unltd-original-imafskygbhdbhkts.jpeg?q=50" />
				  <Card.Body>
				    <Card.Title>Jean brown</Card.Title>
				    <Card.Text>
				     Descrition: Leggings
				    </Card.Text>
				    <Card.Text>
				    php 500
				    </Card.Text>
				     <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3}  className="pt-3" >
				<Card style={{ width: '18rem' }}  className="cardStyle">
				  <Card.Img  className="imgCard" variant="top" src="https://e7.pngegg.com/pngimages/590/245/png-clipart-subwoofer-jl-audio-vehicle-audio-ohm-bass-speaker-electronics-speaker.png" />
				  <Card.Body>
				    <Card.Title>Bass speaker</Card.Title>
				    <Card.Text>
				    	Descrition: sound System
				    </Card.Text>
				    <Card.Text>
				    	php 10,999
				    </Card.Text>
				      <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3} className="pt-3">
				<Card style={{ width: '18rem' }}>
				  <Card.Img  className="imgCard" variant="top" src="https://pisces.bbystatic.com/image2/BestBuy_US/images/products/6428/6428998_sd.jpg" />
				  <Card.Body>
				    <Card.Title>Laptop</Card.Title>
				    <Card.Text>
				     Descrition	: Lenovo
				    </Card.Text>
				    <Card.Text>
				    php 14,999
				    </Card.Text>
				     <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3} className="pt-3">
				<Card style={{ width: '18rem' }}>
				  <Card.Img  className="imgCard" variant="top" src="https://trek.scene7.com/is/image/TrekBicycleProducts/MadoneSLR9_22_35716_D_Primary?$responsive-pjpg$&cache=on,on&wid=1920&hei=1440" />
				  <Card.Body>
				    <Card.Title>Bike</Card.Title>
				    <Card.Text>
				     Descrition: Mountain Bike
				    </Card.Text>
				    <Card.Text>
				    php 13,500
				    </Card.Text>
				     <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3} className="pt-3">
				<Card style={{ width: '18rem' }}>
				  <Card.Img  className="imgCard" variant="top" src="https://images.saymedia-content.com/.image/t_share/MTc0NDQ0NzYwNzkzMDk3NTc2/a-dads-guide-to-buying-kids-toys.jpg" />
				  <Card.Body>
				    <Card.Title>Kids toys</Card.Title>
				    <Card.Text>
				     Descrition: Kids toys
				    </Card.Text>
				    <Card.Text>
				    php 299
				    </Card.Text>
				    <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
			<Col xs={12} md={3} className="pt-3">
				<Card style={{ width: '18rem' }}>
				  <Card.Img  className="imgCard" variant="top" src="https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F43%2F2021%2F04%2F24%2FAnolon-Nouvelle-Stainless-10-Piece-Cookware.jpg" />
				  <Card.Body>
				    <Card.Title>Cooking Ware</Card.Title>
				    <Card.Text>
				     Descrition: 1 Set of cooking ware
				    </Card.Text>
				     <Card.Text>
				    php 999
				    </Card.Text>
				   <Button variant="primary" style={{width:220}}>Add to cart</Button>
				  </Card.Body>
				</Card>
				
			</Col>
		</Row>
		</Container>
		 </Fragment>

		)
}