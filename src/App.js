import { Container, } from 'react-bootstrap';

import { Fragment } from 'react';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from './pages/Admin';
import { UserProvider } from './UserContext';
import {useState, useEffect } from 'react';


// import ProductList from './pages/Home'
//import {Route, Switch} from 'react-router-dom';
import './App.css';



function App() {

  const [user, setUser] = useState({
  id: null,
  isAdmin:null
  })

  const unsetUser =() =>{
    localStorage.clear();
  }
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    },[user])
  return (
  
         <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
          <AppNavbar />
            <Switch>
               <Route exact path="/" component ={Home}/>
                <Route exact path="/register" component ={Register}/>
                <Route exact path="/login" component ={Login}/>
                 <Route exact path="/admin" component ={Admin}/>
              {/*<Route exact path="/courses" component ={Courses}/>
              <Route exact path="/courses/:courseId" component ={CourseView}/>
              <Route exact path="/login" component ={Login}/>
              <Route exact path="/logout" component ={Logout}/>
              <Route exact path="/register" component ={Register}/>
              <Route component={Error} />*/}
            </Switch>
          </Router>
        </UserProvider>
    
  );
}

export default App;
